ARG logstash_version
FROM docker.elastic.co/logstash/logstash:${logstash_version}
RUN sed --in-place "s/gem.add_runtime_dependency \"sinatra\", '~> 2'/gem.add_runtime_dependency \"sinatra\", '~> 2.1.0'/g" /usr/share/logstash/logstash-core/logstash-core.gemspec
RUN /usr/share/logstash/bin/ruby -S /usr/share/logstash/vendor/bundle/jruby/2.5.0/bin/bundle install
RUN bin/logstash-plugin install --preserve logstash-input-sqs \ 
    && bin/logstash-plugin install --preserve logstash-output-elasticsearch \ 
    && bin/logstash-plugin install --preserve logstash-filter-json_encode \ 
    && bin/logstash-plugin install --preserve logstash-filter-json \ 
    && bin/logstash-plugin install --preserve logstash-filter-grok \ 
    && bin/logstash-plugin install --preserve logstash-filter-mutate 
